package be.variaNT.java.projectTimeSheetApp;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class Simulation {
    Keyboard keyboard = new Keyboard();
    DateTimeFormatter dayFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    DateTimeFormatter hourFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
    int menuInputChoice;
    WeekServiceImpl weekService = new WeekServiceImpl();

    public void startSimulation() throws Exception {
        LocalDate now = LocalDate.now();
        weekService.createWeek("04/11/2020");
        Processor processor = new Processor();
        Menu menu = new Menu();
        menu.bootUpMenu();
        menu.inBetween();

        boolean wantsToStay = true;

        while (wantsToStay) {
            menu.mainMenu();
            int inputChoice;
            inputChoice = tryMenuInputChoice();
            menu.inBetween();
            switch (inputChoice) {
                case 1:
                    processor.printRates();
                    menu.inBetween();
                    break;
                case 2:
                    System.out.println("Please enter the start of your work week in format: DD/MM/YYYY");
                    String menuInputDate = tryMenuInputDate();
                    this.weekService.createWeek(menuInputDate);
                    menu.inBetween();
                    break;
                case 3:
                    System.out.printf("Please choose if you'd like to enter a work shift or a break.%n" +
                            "1. Work shift %n" +
                            "2. Break %n");
                    int choiceAddWorkOrBreak = tryToInput1or2();
                    if (choiceAddWorkOrBreak == 1) {
                        System.out.println("Please enter: Date (DD/MM/YYYY)");
                        String inputDate = tryToCheckIfDayIsInWeek();
                            String[] hours = tryToCheckIfEndHourIsBeforeStartHour();
                            System.out.println("Please enter description");
                            String inputDescription = keyboard.askForText();
                            weekService.getWeek().addWorkSlotToDay(inputDate, hours[0],hours[1], inputDescription);
                            System.out.format("You have made the following work shift: %n" +
                                    "%s %n" +
                                    "%s %n" +
                                    "%s %n" +
                                    "%s %n", inputDate, hours[0], hours[1], inputDescription);

                    } else if (choiceAddWorkOrBreak == 2) {
                        System.out.println("Please enter: Date (DD/MM/YYYY)");
                        String inputDate = tryToCheckIfDayIsInWeek();
                            String[] hours = tryToCheckIfEndHourIsBeforeStartHour();
                            weekService.getWeek().addBreakSlotToDay(inputDate, hours[0], hours[1]);
                            System.out.format("You have made the following break: %n" +
                                    "%s %n" +
                                    "%s %n" +
                                    "%s %n", inputDate, hours[0], hours[1]);
                    }
                    menu.inBetween();
                    break;
                case 4:
                    System.out.printf("Please choose if you'd like to remove a work shift or a break.%n" +
                            "1. Work shift %n" +
                            "2. Break %n");
                    int choiceRemoveWorkOrBreak = tryToInput1or2();
                    if (choiceRemoveWorkOrBreak == 1) {
                        System.out.println("Please enter: Date (DD/MM/YYYY)");
                        tryToCheckIfDayIsInWeek();
                            System.out.println(weekService.getDayInArray().printAllWorkSlots());
                            System.out.println("Please enter the number of the slot you want to remove.");
                            // TODO check if the chosen workslot exists otherwise exception
                            int workSlotToRemove = keyboard.askForNumber() - 1;
                            weekService.getDayInArray().removeSlot(workSlotToRemove);
                            break;

                    } else if (choiceRemoveWorkOrBreak == 2) {
                        System.out.println("Please enter: Date (DD/MM/YYYY)");
                        tryToCheckIfDayIsInWeek();
                            System.out.println(weekService.getDayInArray().printAllBreakSlots());
                            System.out.println("Please enter the number of the slot you want to remove.");
                            // TODO check if chosen breakslot exist otherwise exception
                            int breakSlotToRemove = keyboard.askForNumber() - 1;
                            weekService.getDayInArray().removeSlot(breakSlotToRemove);
                            break;
                    }
                case 5:
                    System.out.printf("Please choose if you'd like to reset the start day of your week. %n" +
                            "Or just remove all days in your week %n" +
                            "1. reset start date %n" +
                            "2. Remove all days in week %n");
                    int choiceRemoveOrResetWeek = tryToInput1or2();
                    if (choiceRemoveOrResetWeek == 1) {
                        System.out.println("Please enter the new start date. DD/MM/YYYY %n");
                        String newStartDate = tryMenuInputDate();
                        weekService.resetWeek(newStartDate);
                    } else if (choiceRemoveOrResetWeek == 2) {
                        weekService.emptyWeek();
                    }
                    break;
                case 6:
                    System.out.println("Which day would you like to get a paycheck of? Please enter DD/MM/YYYY");
                    tryToCheckIfDayIsInWeek();
                        weekService.getDayInArray().printAllSlotsWithPaycheck();
                        menu.inBetween();
                    break;
                case 7:
                    System.out.println("Your paycheck for this week: ");
                    weekService.getWeek().printAllSlotsWithPaycheckForEachDay();
                    menu.inBetween();
                    break;
                case 8:
                    menu.closeDownMenu();
                    wantsToStay = false;
                    break;
            }

        }
    }

    public void setMenuInputChoice(int menuInputChoice) {
        this.menuInputChoice = menuInputChoice;
    }

    public int getMenuInputChoice() {
        return this.menuInputChoice;
    }


    public int tryMenuInputChoice() {
        try {
            setMenuInputChoice(keyboard.askForNumber());
        } catch (NumberFormatException e) {
            System.out.println("Enter a number between 1 and 8.");
            tryMenuInputChoice();
        }
        if (getMenuInputChoice() < 1 || getMenuInputChoice() > 8) {
            System.out.println("Number is not between 1 and 8");
            tryMenuInputChoice();
        }
        return getMenuInputChoice();
    }

    public int tryToInput1or2() {
        int choiceWorkOrBreakSlot;
        try {
            choiceWorkOrBreakSlot = keyboard.askForNumber();
        } catch (NumberFormatException e) {
            System.out.println("Please enter 1 or 2");
            choiceWorkOrBreakSlot = tryToInput1or2();
        }
        if (choiceWorkOrBreakSlot < 1 || choiceWorkOrBreakSlot > 2) {
            System.out.println("Number is not 1 or 2. Please enter 1 or 2");
            choiceWorkOrBreakSlot = tryToInput1or2();
        }
        return choiceWorkOrBreakSlot;
    }

    public String tryMenuInputDate() {
        String inputDate = keyboard.askForText();
        try {
            LocalDate.parse(inputDate, dayFormatter);
        } catch (DateTimeParseException dtpe) {
            System.out.println("Please enter the date in the format DD/MM/YYYY");
            inputDate = tryMenuInputDate();
        }
        return inputDate;
    }

    public String tryMenuInputTime() {
        String inputTime = keyboard.askForText();
        try {
            LocalTime.parse(inputTime, hourFormatter);
        } catch (DateTimeParseException dtpe) {
            System.out.println("Please enter the time in the format HH:MM:SS");
            inputTime = tryMenuInputTime();
        }
        return inputTime;
    }

    public String tryToCheckIfDayIsInWeek() {
        String dayInWeek = tryMenuInputDate();
        if (!weekService.getWeek().checkIfDayIsInWeek(dayInWeek)) {
            System.out.println("The day you entered is not in this week");
            System.out.println("Please enter a day in your week in format DD/MM/YYYY");
            dayInWeek = tryToCheckIfDayIsInWeek();
        }
        return dayInWeek;
    }

    public String[] tryToCheckIfEndHourIsBeforeStartHour(){
        String[] hours = new String[2];
        System.out.println("Please enter: Start hour (HH:MM:SS)");
        hours[0] = tryMenuInputTime();
        System.out.println("Please enter: End hour (HH:MM:SS)");
        hours[1] = tryMenuInputTime();
        LocalTime startHour = LocalTime.parse(hours[0], hourFormatter);
        LocalTime endHour = LocalTime.parse(hours[1], hourFormatter);
        if (endHour.isBefore(startHour)) {
            System.out.println("The end hour can't be before the start hour");
            hours = tryToCheckIfEndHourIsBeforeStartHour();
        }
        return hours;
    }

//    // TODO
//    public int tryCheckIfSlotIsInDay(){
//
//    }

}
