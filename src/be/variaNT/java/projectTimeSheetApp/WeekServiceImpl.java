package be.variaNT.java.projectTimeSheetApp;

import java.util.Arrays;

public class WeekServiceImpl implements WeekService{
    private WorkedWeek workedWeek;


    @Override
    public void resetWeek(String newStartOfWorkWeek) {
        createWeek(newStartOfWorkWeek);
    }

    @Override
    public void emptyWeek () {
        Arrays.fill(this.workedWeek.getWorkedDays(), null);
    }


    public void resetAllSlotsInWeek() {
        for (int i = 0; i < workedWeek.getWorkedDays().length; i++){
            Arrays.fill(workedWeek.getWorkedDays()[i].getSlots(), null);
        }
    }

    @Override
    public void createWeek(String startOfWorkWeek) {
        this.workedWeek = new WorkedWeek(startOfWorkWeek);
        this.workedWeek.setWeek();
    }


    @Override
    public WorkedWeek getWeek() {
        return this.workedWeek;
    }

    public WorkedDay getWorkedDay (int indexWorkDay) {
        return this.getWeek().getWorkedDays()[indexWorkDay];
    }

    public Slot getSlot(int indexWorkDay, int indexSlot) {
        return this.getWorkedDay(indexWorkDay).getSlots()[indexSlot];
    }

    public int getIndexOfDayInArray() {
        return this.getWeek().getDayInArray();
    }

    public WorkedDay getDayInArray(){
        return this.getWorkedDay(this.getIndexOfDayInArray());
    }
}
