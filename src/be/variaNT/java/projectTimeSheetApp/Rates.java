package be.variaNT.java.projectTimeSheetApp;

public enum Rates {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY;

    double overTimeHourlyRate;

    double normalHourlyRate;

    void  setoverTimeHourlyRate(double overTimeHourlyRate) {
     this.overTimeHourlyRate = overTimeHourlyRate;
    }

    double getOverTimeHourlyRate() {

        return overTimeHourlyRate;
    }

    void setNormalHourlyRate(double normalHourlyRate) {
        this.normalHourlyRate = normalHourlyRate;
    }

    double getNormalHourlyRate() {
        return normalHourlyRate;
    }




}
