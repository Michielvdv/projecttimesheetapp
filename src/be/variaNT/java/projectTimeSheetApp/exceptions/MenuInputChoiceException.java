package be.variaNT.java.projectTimeSheetApp.exceptions;

public class MenuInputChoiceException extends Exception {

    public MenuInputChoiceException(){
        this("The number must be between 1 and 8");
    }

    public MenuInputChoiceException(String message){
        super(message);
    }

    public MenuInputChoiceException(String message, Throwable cause){

    }

    public MenuInputChoiceException(Throwable cause){
        super("The number must be between 1 and 8", cause);
    }
}
