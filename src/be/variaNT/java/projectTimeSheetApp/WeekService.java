package be.variaNT.java.projectTimeSheetApp;

public interface WeekService {

    void resetWeek(String newStartOfWorkWeek);

    void emptyWeek ();

    void createWeek(String startOfWorkWeek);

    WorkedWeek getWeek();


}
