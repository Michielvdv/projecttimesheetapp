package be.variaNT.java.projectTimeSheetApp;

//TODO Als user slot ingeeft dat op 2 dagen valt, deze opdelen en in andere dag steken.
//TODO Als begin uur voor eind uur valt, FOUTMELDING

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Objects;

public class WorkedDay implements Day {
    LocalDate startOfWorkDay;
    LocalTime startOfWorkDayHour;
    LocalTime endOfWorkDayHour;
    Slot[] slots;
    int count = 0;
    Processor processor = new Processor();


    DateTimeFormatter dayFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    DateTimeFormatter hourFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    public WorkedDay(String startOfWorkDay) {
        setStartOfWorkDay(LocalDate.parse(startOfWorkDay, dayFormatter));
        this.slots = new Slot[20];
    }

    public WorkedDay(LocalDate startOfWorkDay) {
        setStartOfWorkDay(startOfWorkDay);
        this.slots = new Slot[20];
    }

    public void setStartOfWorkDay(LocalDate startOfWorkDay) {
        this.startOfWorkDay = startOfWorkDay;
    }

    public LocalDate getStartOfWorkDay() {
        return startOfWorkDay;
    }

    public void setStartOfWorkDayHour(String startOfWorkDayHour) {
        this.startOfWorkDayHour = LocalTime.parse(startOfWorkDayHour, hourFormatter);
    }

    public LocalTime getStartOfWorkDayHour() {
        return startOfWorkDayHour;
    }

    public void setEndOfWorkDayHour(String startOfWorkDayHour) {
        LocalTime temporaryInput = LocalTime.parse(startOfWorkDayHour, hourFormatter);
        this.endOfWorkDayHour = temporaryInput.plusHours(8);
    }

    LocalTime getEndOfWorkDayHour() {
        return endOfWorkDayHour;
    }


    public WorkSlot makeWorkSlot(String startHour, String endHour, String description) {
        WorkSlot workslot = new WorkSlot(startHour, endHour, description);
        return workslot;
    }

    public BreakSlot makeBreakSlot(String startHour, String endHour) {
        BreakSlot breakSlot = new BreakSlot(startHour, endHour);
        return breakSlot;
    }

    void addWorkSlot(Slot workSlot) {
        try {
            slots[count++] = workSlot;
        } catch (ArrayIndexOutOfBoundsException outOfBoundsException) {
            System.out.println("You have entered too many work slots!");
            outOfBoundsException.printStackTrace();
        }

    }

    void addBreakSlot(Slot breakSlot) {
        try {
            slots[count++] = breakSlot;
        } catch (ArrayIndexOutOfBoundsException outOfBoundsException) {
            System.out.println("You have entered too many break slots!");
            outOfBoundsException.printStackTrace();
        }

    }

    void removeSlot(int index) {
        for (int i = 0; i < slots.length; i++) {
            if (this.getSlots()[index].equals(slots[i])) {
                slots[i] = null;
                break;
            }
        }
    }

    public Slot[] getSlots() {
        return this.slots;
    }

    public Slot[] getAllWorkSlots() {
        int count = 0;
        int count2 = 0;
        for (int i = 0; i < this.getSlots().length; i++) {
            if (this.getSlots()[i] instanceof WorkSlot) {
                count++;
            }
        }
        Slot[] temporaryArray = new Slot[count];

        for (Slot slot : this.getSlots()) {
            if (slot instanceof WorkSlot) {
                temporaryArray[count2++] = slot;
            }

        }
        return temporaryArray;
    }

    public boolean checkIfWorkSlotIsInDay(int inputSlotInDay){
        boolean isSlotinDay = false;
        for (int i = 0; i < this.getAllWorkSlots().length; i++) {
            if (this.getAllWorkSlots()[inputSlotInDay].equals(this.getAllWorkSlots()[i])){
                isSlotinDay = true;
                break;
            }
        }
        return isSlotinDay;
    }

    public Slot[] getAllBreakSlots() {
        int count = 0;
        int count2 = 0;
        for (int i = 0; i < this.getSlots().length; i++) {
            if (this.getSlots()[i] instanceof BreakSlot) {
                count++;
            }
        }
        Slot[] temporaryArray = new Slot[count];

        for (Slot slot : this.getSlots()) {
            if (slot instanceof BreakSlot) {
                temporaryArray[count2++] = slot;
            }

        }
        return temporaryArray;
    }

    public StringBuilder printAllWorkSlots() {
        StringBuilder sb = new StringBuilder();
        for (Slot workSlot : getAllWorkSlots()) {
            sb.append(workSlot);
        }
        return sb;
    }

    public StringBuilder printAllBreakSlots() {
        StringBuilder sb = new StringBuilder();
        for (Slot breakSlot : getAllBreakSlots()) {
            sb.append(breakSlot);
        }
        return sb;
    }

    public Slot[] getAllFilledInSlots() {
        int count = 0;
        int count2 = 0;
        for (int i = 0; i < slots.length; i++) {
            if (slots[i] != null) {
                count++;
            }
        }
        Slot[] temporaryArray = new Slot[count];
        for (int i = 0; i < slots.length; i++) {
            if (slots[i] != null) {
                temporaryArray[count2] = slots[i];
                count2++;
            }
        }
        return temporaryArray;
    }

    public void printAllSlotsWithPaycheck() {
        int totalMinutes = 0;
        for (Slot slot : this.getAllFilledInSlots()) {
            totalMinutes += (int) slot.getTotalMinutes();
        }
        System.out.format("%1$td/%1$tm/%1$tY %n", this.getStartOfWorkDay());
        for (Slot slot : this.getAllFilledInSlots()) {
            System.out.printf("%-20s %s %n" +
                            "%-20s %s %n" +
                            "%-20s %s %n" +
                            "%-20s € %.2f %n" +
                            "------------------------------------ %n"
                    ,"Slot", slot instanceof WorkSlot ? "Workslot" : "Breakslot"
                    ,"Starting hour:", slot.getStart()
                    ,"Ending hour:", slot.getEnd()
                    ,"Payment slot:", processor.calculateSlotPayment(this, slot));

        }
        System.out.printf("%-20s %.2f %n" +
                        "%-20s € %.2f %n" +
                        "%-20s € %.2f %n" +
                        "%-20s € %.2f %n" +
                        "------------------------------------ %n"
                ,"Total hours:", processor.convertMinutesToHours(totalMinutes)
                ,"Bruto:", processor.getPaymentByDay(this)
                ,"Netto:", processor.calculateNettoPayment(processor.getPaymentByDay(this))
                ,"Tax:", processor.calculateTax(processor.getPaymentByDay(this)));
    }


        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            for (Slot slot : getAllFilledInSlots()) {
                sb.append(slot);
            }
            return "*****WorkedDay*****" + "\n" +
                    startOfWorkDay + "\n" +
                    sb;
        }

        @Override
        public boolean equals (Object o){
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            WorkedDay day = (WorkedDay) o;
            return count == day.count &&
                    Objects.equals(getStartOfWorkDay(), day.getStartOfWorkDay()) &&
                    Objects.equals(getStartOfWorkDayHour(), day.getStartOfWorkDayHour()) &&
                    Objects.equals(getEndOfWorkDayHour(), day.getEndOfWorkDayHour()) &&
                    Arrays.equals(slots, day.slots) &&
                    Objects.equals(dayFormatter, day.dayFormatter) &&
                    Objects.equals(hourFormatter, day.hourFormatter);
        }

        @Override
        public int hashCode () {
            int result = Objects.hash(getStartOfWorkDay(), getStartOfWorkDayHour(), getEndOfWorkDayHour(), count, dayFormatter, hourFormatter);
            result = 31 * result + Arrays.hashCode(slots);
            return result;
        }
    }


