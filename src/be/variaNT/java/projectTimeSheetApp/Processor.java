package be.variaNT.java.projectTimeSheetApp;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class Processor {
    // Fields


    // Constructor
    // Als we een processor aanmaken moeten we een WorkedWeek meegeven.
    public Processor() {

    }

    public int getRates(WorkedDay workedDay) {
        int rate = 0;

            if (workedDay.getStartOfWorkDay().getDayOfWeek().toString().equals(Rates.SATURDAY.toString())) {
                rate = 25;
            } else if (workedDay.getStartOfWorkDay().getDayOfWeek().toString().equals(Rates.SUNDAY.toString())) {
                rate = 35;
            } else {
                rate = 15;
            }

        return rate;
    }

    public void printRates() {
        System.out.printf("Weekdays:%n" +
                "Monday to friday between 08:00 and 18:00 = 15.00/hour%n" +
                "Monday to friday between 0:00 and 08:00 = 20.00/hour%n" +
                "Monday to friday between 18:00 and 0:00 = 20.00/hour %n" +
                "Weekend:%n" +
                "Saturday = 25.00/hour%n" +
                "Sunday = 35.00/hour %n");
    }

    public double calculateSaturdayPayment(WorkedDay workedDay) {
        double totalPayment = 0;
        for (Slot saturdaySlot : workedDay.slots) {
            totalPayment += calculateSaturdaySlotPayment(workedDay, saturdaySlot);
        }
        return totalPayment;
    }

    public double calculateSaturdaySlotPayment(WorkedDay workedDay, Slot saturdaySlot) {
        double saturdaySlotPayment = 0;
        if (saturdaySlot instanceof WorkSlot) {
            double normalHours = ChronoUnit.MINUTES.between(saturdaySlot.getStart(), saturdaySlot.getEnd());
            saturdaySlotPayment = (normalHours / 60) * getRates(workedDay);
        }
        return saturdaySlotPayment;
    }

    public double calculateSundayPayment(WorkedDay workedDay){
        double totalPayment = 0;
        for (Slot sundaySlot : workedDay.slots) {
            totalPayment += calculateSundaySlotPayment(workedDay, sundaySlot);
        }
        return totalPayment;
    }

    public double calculateSundaySlotPayment(WorkedDay workedDay, Slot sundaySlot){
        double sundaySlotPayment = 0;
        if (sundaySlot instanceof WorkSlot) {
                double normalHours = ChronoUnit.MINUTES.between(sundaySlot.getStart(), sundaySlot.getEnd());
                sundaySlotPayment = (normalHours / 60) * getRates(workedDay);
        }
        return sundaySlotPayment;
    }

    public double calculateWeekdayPayment(WorkedDay workedDay){
        double totalPayment = 0;
        for (Slot slot : workedDay.slots) {
            totalPayment += calculateSlotPayment(workedDay, slot);
        }
        return totalPayment;
    }

    public double calculateSlotPayment(WorkedDay workedDay, Slot slot) {
        double totalPaymentSlot =0;
        if (slot instanceof WorkSlot) {
            if (slot.getStart().isAfter(WorkSlot.WORKDAY_START_HOUR) && slot.getEnd().isBefore(WorkSlot.WORKDAY_END_HOUR)) {
                totalPaymentSlot += calculateRegularSlotPayment(workedDay, slot);
            } else if (slot.getStart().isBefore(WorkSlot.WORKDAY_START_HOUR)) {
                totalPaymentSlot += calculateMorningOvertimeSlotPayment(workedDay, slot);

            } else if (slot.getEnd().isAfter(WorkSlot.WORKDAY_END_HOUR)) {
                totalPaymentSlot += calculateEveningOvertimeSlotPayment(workedDay, slot);
            } else if (slot.getStart().isBefore(WorkSlot.WORKDAY_START_HOUR) && slot.getEnd().isAfter(WorkSlot.WORKDAY_END_HOUR)) {
                totalPaymentSlot += calculateMorningAndEveningOvertimeSlotPayment(workedDay, slot);
            }
        }
        return totalPaymentSlot;
    }


    public double calculateRegularSlotPayment(WorkedDay workedDay, Slot slot){
        double regularPayment = 0;
        double normalHours = ChronoUnit.MINUTES.between(slot.getStart(), slot.getEnd());
        return regularPayment = (normalHours / 60) * getRates(workedDay);
    }

    public double calculateMorningOvertimeSlotPayment(WorkedDay workedDay, Slot slot){
        double morningOvertimePayment = 0;
        double normalHoursWithOvertime = ChronoUnit.MINUTES.between(slot.getStart(), slot.getEnd());
        double overtimeInMorning = Math.abs(ChronoUnit.MINUTES.between(slot.getStart(), WorkSlot.WORKDAY_START_HOUR));
        double totalNormalHoursWithoutOvertime = normalHoursWithOvertime - overtimeInMorning;
        double totalPaymentOverTimeRate = (overtimeInMorning / 60) * 20;
        double totalPaymentNormalRate = (totalNormalHoursWithoutOvertime / 60) * getRates(workedDay);
        return morningOvertimePayment = totalPaymentOverTimeRate + totalPaymentNormalRate;
    }

    public double calculateEveningOvertimeSlotPayment(WorkedDay workedDay, Slot slot){
        double eveningOvertimePayment = 0;
        double normalHoursWithOvertime = ChronoUnit.MINUTES.between(slot.getStart(), slot.getEnd());
        double overtimeInEvening = Math.abs(ChronoUnit.MINUTES.between(slot.getEnd(), WorkSlot.WORKDAY_END_HOUR));
        double totalNormalHoursWithoutOverTime = normalHoursWithOvertime - overtimeInEvening;
        double totalPaymentOverTimeRate = (overtimeInEvening / 60) * 20;
        double totalPaymentNormalRate = (totalNormalHoursWithoutOverTime / 60) * getRates(workedDay);
        return eveningOvertimePayment = totalPaymentOverTimeRate + totalPaymentNormalRate;
    }

    public double calculateMorningAndEveningOvertimeSlotPayment(WorkedDay workedDay, Slot slot){
        double morningAndEveningOvertimePayment = 0;
        double normalHoursWithOvertime = ChronoUnit.MINUTES.between(slot.getStart(), slot.getEnd());
        double overtimeInEvening = Math.abs(ChronoUnit.MINUTES.between(slot.getEnd(), WorkSlot.WORKDAY_END_HOUR));
        double overtimeInMorning = Math.abs(ChronoUnit.MINUTES.between(slot.getStart(), WorkSlot.WORKDAY_START_HOUR));
        double totalNormalHoursWithoutOverTime = normalHoursWithOvertime - overtimeInEvening - overtimeInMorning;
        double totalPaymentOverTimeRate = ((overtimeInEvening + overtimeInMorning) / 60) * 20;
        double totalPaymentNormalRate = (totalNormalHoursWithoutOverTime / 60) * getRates(workedDay);
        return morningAndEveningOvertimePayment = totalPaymentOverTimeRate + totalPaymentNormalRate;
    }

    public double getPaymentByDay(WorkedDay workedDay) {
        double totalPayment = 0;
        if (workedDay.getStartOfWorkDay().getDayOfWeek().toString().equals(Rates.SATURDAY.toString())) {
            totalPayment += calculateSaturdayPayment(workedDay);
        }
        else if (workedDay.getStartOfWorkDay().getDayOfWeek().toString().equals(Rates.SUNDAY.toString())) {
            totalPayment += calculateSundayPayment(workedDay);
        }
        else {
            totalPayment += calculateWeekdayPayment(workedDay);
        }
        return totalPayment;
    }

    public double convertMinutesToHours( int minutes) {
        return (double) minutes / 60;
    }

    public int convertHoursToMinutes (double hours) {
        return (int) hours * 60;
    }

    public double calculateNettoPayment (double bruto) {
        return bruto - calculateTax(bruto);
    }

    public double calculateTax (double bruto) {
        return ((bruto / 100) * 21);
    }

    public double calculateWeekBruto (WorkedWeek workedWeek) {
        double totalBruto = 0;
        for (WorkedDay day : workedWeek.getWorkedDays()) {
            totalBruto += calculateWeekdayPayment(day);
        }
        return totalBruto;
    }

    public double calculateWeekNetto (double bruto) {
        return calculateNettoPayment(bruto);
    }

    public double calculateWeekTax (double bruto){
        return calculateTax(bruto);
    }
}





