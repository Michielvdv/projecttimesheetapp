package be.variaNT.java.projectTimeSheetApp;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Objects;

public class WorkSlot implements Slot{
    String description;
    long[] minutesByType;
    LocalTime start;
    LocalTime end;
    long totalMinutes;

    DateTimeFormatter hourFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");


    public WorkSlot(String startString, String endString){
        setDescription("Workslot");
        setStart(convertStringToLocalTime(startString));
        setEnd(convertStringToLocalTime(endString));
        setTotalMinutes(getStart(), getEnd());
    }

    public WorkSlot(String startString, String endString, String description){
        setDescription(description);
        setStart(convertStringToLocalTime(startString));
        setEnd(convertStringToLocalTime(endString));
        setTotalMinutes(getStart(), getEnd());
    }

    public WorkSlot(LocalTime start, LocalTime end) {
        this(start, end, "Workslot");
    }

    public WorkSlot(LocalTime start, LocalTime end, String description){
        setDescription(description);
        setStart(start);
        setEnd(end);
        setTotalMinutes(start, end);
    }

    @Override
    public void printSlotInfo() {


    }

    @Override
    public void setDescription(String description) {
    this.description = description;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public void setMinutesByType(long[] minutesByType) {
    this.minutesByType = minutesByType;
    }

    @Override
    public long[] getMinutesByType() {
        return minutesByType;
    }

    @Override
    public void setStart(LocalTime start) {
    this.start = start;
    }

    @Override
    public LocalTime getStart() {
        return start;
    }

    @Override
    public void setTotalMinutes(LocalTime start, LocalTime end) {
        this.totalMinutes = ChronoUnit.MINUTES.between(start, end);
    }

    @Override
    public long getTotalMinutes() {
        return totalMinutes;
    }

    @Override
    public void setEnd(LocalTime end) {
    this.end = end;
    }

    @Override
    public LocalTime getEnd() {
        return end;
    }

    public LocalTime convertStringToLocalTime(String input){
        LocalTime temporayinput = LocalTime.parse(input, hourFormatter);
        return temporayinput;
    }

    @Override
    public String toString() {
        return String.format("************** %n" +
                "%1$s: %n" +
                "Starting hour: %2$tH:%2$tM:%2$tS %n" +
                "End hour: %3$tH:%3$tM:%3$tS %n" +
                "Total minutes: %4$d %n" +
                "************** %n", description, start, end, totalMinutes);

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WorkSlot workSlot = (WorkSlot) o;
        return getTotalMinutes() == workSlot.getTotalMinutes() &&
                Objects.equals(getDescription(), workSlot.getDescription()) &&
                Arrays.equals(getMinutesByType(), workSlot.getMinutesByType()) &&
                Objects.equals(getStart(), workSlot.getStart()) &&
                Objects.equals(getEnd(), workSlot.getEnd());
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(getDescription(), getStart(), getEnd(), getTotalMinutes());
        result = 31 * result + Arrays.hashCode(getMinutesByType());
        return result;
    }
}
