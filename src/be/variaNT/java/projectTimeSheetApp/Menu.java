package be.variaNT.java.projectTimeSheetApp;

public class Menu {
    final static int QUIT_NUMBER = -1;

    public void bootUpMenu() {
        System.out.printf("Welcome to our Time Sheet App %n" +
                "We are booting up the System for you %n" );
    }

    public void closeDownMenu() {
        System.out.printf("Thank you for using our app %n" +
                "We will close down everything and clean up for you. %n" +
                "See you soon! %n");
    }

    public void inBetween() {
        System.out.printf("------------------------------------ %n");
    }

    public void printChoice(int choice){
        System.out.printf("Your choice: %d", choice);
    }

    public void mainMenu() {
        System.out.printf("What do you want to do? %n" +
                "Type in the number that corresponds with your choice %n" +
                "1. Show the differenty Hourly Rates %n" +
                "2. Start a new workweek %n" +
                "3. Add a worked moment or break %n" +
                "4. Remove a worked moment or break %n" +
                "5. Reset %n" +
                "6. Print paycheck %n" +
                "7. Print detailed paycheck %n" +
                "8. Quit application %n" +
                "Please enter your choice: ");
    }
}
