package be.variaNT.java.projectTimeSheetApp;

import java.time.LocalTime;

public interface Slot {
    LocalTime WORKDAY_START_HOUR = LocalTime.of(8,00,00);
    LocalTime WORKDAY_END_HOUR = LocalTime.of(18,00,00);

    void printSlotInfo();

    void setDescription(String description);

    String getDescription();

    void setMinutesByType(long[] minutesByType);

    long[] getMinutesByType();

    void setStart(LocalTime startTime);

    LocalTime getStart();

    void setTotalMinutes(LocalTime startTime, LocalTime endTime);

    long getTotalMinutes();

    void setEnd(LocalTime endTime);

    LocalTime getEnd();
}
