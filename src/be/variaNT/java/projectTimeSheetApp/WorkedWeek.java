package be.variaNT.java.projectTimeSheetApp;

import java.time.LocalDate;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;

public class WorkedWeek implements Week{
    // Fields
    LocalDate startOfWeek;
    LocalDate endOfWorkWeek;
    WorkedDay[] workedDays = new WorkedDay[7];
    int dayInArray = 0;
    Processor processor = new Processor();

    DateTimeFormatter weekFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    // Constructor
    public WorkedWeek(String startOfWorkWeek){
        setStartOfWorkWeek(startOfWorkWeek);
        setEndOfWorkWeek(startOfWorkWeek);
    }

    // Methods
    void setStartOfWorkWeek(String startOfWorkWeek){
        this.startOfWeek = LocalDate.parse(startOfWorkWeek, weekFormatter);
    }

    LocalDate getStartOfWorkWeek(){
        return startOfWeek;
    }

    void setEndOfWorkWeek(String startOfWorkWeek){
        LocalDate temporayInput = LocalDate.parse(startOfWorkWeek, weekFormatter);

        this.endOfWorkWeek = temporayInput.plusDays(7);
    }

    LocalDate getEndOfWorkWeek(){
        return  endOfWorkWeek;
    }
    //  AS USER: Fill in startOfWeekDay. Then FillArray with weekdays
    public void setWeek () {
        for (int i = 0; i < 7; i++){
            workedDays[i] = new WorkedDay(startOfWeek.plusDays(i));
        }
    }

    public WorkedDay[] getWorkedDays() {
        return workedDays;
    }
    public void setDayinArray(int dayInArray){
        this.dayInArray = dayInArray;
    }

    public int getDayInArray(){
        return this.dayInArray;
    }


    public boolean checkIfDayIsInWeek(String inputDate){
        boolean isDayOfWeek = false;
        LocalDate localDate = LocalDate.parse(inputDate, weekFormatter);
        for (int i = 0; i < workedDays.length; i++) {
        if (localDate.equals(this.workedDays[i].getStartOfWorkDay())){
            isDayOfWeek = true;
            setDayinArray(i);
            }
        }
        return isDayOfWeek;
    }


    public void addWorkSlotToDay(String inputDate, String inputStartHour, String inputEndHour, String inputdescription){
        if (checkIfDayIsInWeek(inputDate)){
            WorkSlot workSlot = new WorkSlot(inputStartHour, inputEndHour, inputdescription);
            workedDays[getDayInArray()].addWorkSlot(workSlot);
        }
    }

    public void addBreakSlotToDay(String inputDate,  String inputStartHour, String inputEndHour){
        if (checkIfDayIsInWeek(inputDate)){
           BreakSlot breakSlot = new BreakSlot(inputStartHour, inputEndHour);
           workedDays[getDayInArray()].addBreakSlot(breakSlot);
        }
    }

    public void printAllSlotsWithPaycheckForEachDay() {
        double totalBruto = 0;
        for (WorkedDay workedDay : this.getWorkedDays()) {
            workedDay.printAllSlotsWithPaycheck();
            System.out.println("------------------------------------");
        }
        System.out.printf("%-28s € %.2f %n" +
                "%-28s € %.2f %n" +
                "%-28s € %.2f %n"
                ,"Total Bruto for this week:", totalBruto = processor.calculateWeekBruto(this),
                "Total Netto for this week:", processor.calculateWeekNetto(totalBruto),
                "Total Tax for this week:", processor.calculateTax(totalBruto));


    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (WorkedDay workedDay : getWorkedDays()) {
            sb.append(workedDay);
        }
        return "" + sb;
    }


}
