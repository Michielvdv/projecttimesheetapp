package be.variaNT.java.projectTimeSheetApp;

import java.util.Scanner;

public class Keyboard {
    final Scanner keyboard = new Scanner(System.in);


    String askForText () {
        String text = keyboard.next();
        return text;
    }

    int askForNumber () {
        String number = keyboard.next();
        return  Integer.parseInt(number);
    }

    double askForDouble () {
        String doubl = keyboard.next();
        return Double.parseDouble(doubl);
    }

    public void close(){
        keyboard.close();
    }

}
